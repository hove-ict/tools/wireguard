# Wireguard

Add the following to a GitLab project's `.gitlab-ci.yml`:

```yaml
---
include:
  - remote: "https://gitlab.com/hove-ict/tools/wireguard/-/raw/v1.0.0/deploy.yml"

variables:
  WIREGUARD_PRIVATE_KEY: SomePrivateKey  # Best to add as CI/CD variable
  WIREGUARD_PEER_CONFIG: |
    # Some Device
    [Peer]
    PublicKey = SomePublicKey
    AllowedIPs = 10.10.0.2/32

    # Some Other Device
    [Peer]
    PublicKey = SomeOtherPublicKey
    AllowedIPs = 10.10.0.3/32  # Ensure you increment the IP
```

## Server prerequisites

Install the following packages on the host.

```shell
sudo apt install linux-headers-$(uname -r)
```
