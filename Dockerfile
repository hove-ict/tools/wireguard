FROM alpine

RUN apk add --no-cache wireguard-tools

COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
